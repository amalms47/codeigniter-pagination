

//ROUTE

	
	$route['pagination'] = 'Group/Pagination';
	$route['pagination/(:num)'] = 'Group/Pagination/$1';

//CONSTRUCT
	
	$this->load->helper('url');
        $this->load->library("pagination");


//CONTROLLER

  	$this->isAuthorised();
        $config = array();

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>' . "\n";
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>' . "\n";
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>' . "\n";
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>' . "\n";
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>' . "\n";



        $config["base_url"]    = base_url() . "pagination";
        $config["total_rows"]  = $this->GroupModel->get_count();
        $config["per_page"]    = 10;
        $config["uri_segment"] = 2;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $this->loader["links"] = $this->pagination->create_links();
        $this->loader['data']  = $this->GroupModel->getallGroup($config["per_page"], $page);

        
        $this->loader['pageview']="Pagination";
        $this->load->view('common/template',$this->loader);



//MODEL


	    public function get_count() {
		return $this->db->count_all('group_table');
	    }

	    public function get_authors($limit, $start) {
		$this->db->limit($limit, $start);
		$query = $this->db->get('group_table');
		return $query->result_array();
	    }

//VIEW 

	After foreach   echo $links
